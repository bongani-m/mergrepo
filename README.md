# Assignment 3

## Before you start:

If you've worked with a partner, only one of you will submit the assignment in Canvas.

But, first, both of you must do the following.

 ✅ Click on People in the sidebar.
 
 ✅ Click on the tab MergeRequest.
 
 ✅ Both of you must join the same group.
 
 ✅ Then, only one of you will submit the assignment.

## Assignment instructions:

In this assignment you must work with a partner to perform merge request.

You VS your partner

You: create a public repo call it `mergRepo`.

Your partner:

✅ Fork the public repo `mergRepo`
    
✅ Clone `mergRepo` to your local machine.
    
✅ Add a dummy file call it `merge.txt`
    
✅ Add, commit, and push to your remote repo.

✅ Submit a merge request

You:

✅ Accept the merge request

✅ Upload a screenshot of the GitLab 

✅ screen right after you accept the 

✅
merge request and upload it to this assignment.
